import { Component, OnInit, isDevMode } from '@angular/core';

//could have also done the following
//[style.visibility]="visibility ? 'visible' : 'hidden'"

@Component({
  selector: 'nav-toggle-button',
  template: `
    <button class='toggle-button' *ngIf='visibility'>In Dev</button>
  `,
  styles: [
    `
    .toggle-button {
      margin-left: 50px;
      -webkit-border-radius: 2px;
      border-radius: 2px;
      outline: 0 !important;
      -webkit-transition: all ease-in-out .15s;
      -o-transition: all ease-in-out .15s;
      transition: all ease-in-out .15s;
      position: relative;
      padding: 0.65rem 1.25rem;
      cursor: pointer;
      overflow: hidden;
      color: #fff;
      background-color: #46758b !important;
      border-color: #46758b !important;
    }
`
  ]
})


export class NavToggleButtonComponent implements OnInit {
  visibility: boolean = false;

  ngOnInit() {
    this.visibility = isDevMode();    
  }

}
